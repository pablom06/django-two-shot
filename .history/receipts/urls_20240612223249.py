from django.urls import path
from .views import receipt_list

urlpatterns = [
    path('', receipt_list, name='home'),
    path('receipts/', receipt_list, name='receipt_list'),
]
