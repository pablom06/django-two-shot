from django.shortcuts import render, redirect, get_object_or_404
from .models import ExpenseCategory, Account, Receipt
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from .forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from .models import ExpenseCategory, Account, Receipt

@login_required
def receipt_list(request):
    query = request.GET.get('q')
    if query:
        receipts = Receipt.objects.filter(purchaser=request.user, vendor__icontains=query)
    else:
        receipts = Receipt.objects.filter(purchaser=request.user)
    return render(request, 'receipts/receipt_list.html', {'receipts': receipts})

@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')  # Redirect to the receipt list view
    else:
        form = ReceiptForm()
    return render(request, 'receipts/create_receipt.html', {'form': form})

@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(receipts__purchaser=request.user).distinct()
    category_data = []
    for category in categories:
        count = Receipt.objects.filter(category=category, purchaser=request.user).count()
        category_data.append({
              'id': category.id,
            'name': category.name,
            'receipt_count': count
        })
    return render(request, 'receipts/category_list.html', {'categories': category_data})

@login_required
def account_list(request):
    accounts = Account.objects.filter(receipts__purchaser=request.user).distinct()
    account_data = []
    for account in accounts:
        count = Receipt.objects.filter(account=account, purchaser=request.user).count()
        account_data.append({
            'name': account.name,
            'number': account.number,
            'receipt_count': count
        })
    return render(request, 'receipts/account_list.html', {'accounts': account_data})

@login_required
def create_category(request):
    if request.method == 'POST':
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect('category_list')
    else:
        form = ExpenseCategoryForm()
    return render(request, 'receipts/create_category.html', {'form': form})

@login_required
def create_account(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect('account_list')
    else:
        form = AccountForm()
    return render(request, 'receipts/create_account.html', {'form': form})

@login_required
def delete_receipt(request, receipt_id):
    receipt = get_object_or_404(Receipt, id=receipt_id, purchaser=request.user)
    if request.method == 'POST':
        receipt.delete()
        return redirect('home')
    return render(request, 'receipts/delete_receipt.html', {'receipt': receipt})

@login_required
def category_receipts(request, category_id):
    category = get_object_or_404(ExpenseCategory, id=category_id, owner=request.user)
    receipts = Receipt.objects.filter(category=category, purchaser=request.user)
    return render(request, 'receipts/category_receipts.html', {'category': category, 'receipts': receipts})
