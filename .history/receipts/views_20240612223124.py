from django.shortcuts import render, redirect
from .models import ExpenseCategory, Account, Receipt
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from .forms import ReceiptForm

@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {'receipts': receipts}
    return render(request, 'receipts/receipt_list.html', context)
