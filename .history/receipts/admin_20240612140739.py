from django.contrib import admin

class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'owner')

class AccountAdmin(admin.ModelAdmin):
    list_display = ('name', 'number', 'owner')

class TodoListAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')

admin.site.register(TodoList, TodoListAdmin)

ExpenseCategory
Account
Receipt
