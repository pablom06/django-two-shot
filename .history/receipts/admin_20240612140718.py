from django.contrib import admin

class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'name')

class TodoListAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')

admin.site.register(TodoList, TodoListAdmin)

ExpenseCategory
Account
Receipt
