from django.shortcuts import render, redirect
from .models import ExpenseCategory, Account, Receipt
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from .forms import ReceiptForm

@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {'receipts': receipts}
    return render(request, 'receipts/receipt_list.html', context)

@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')  # Redirect to the receipt list view
    else:
        form = ReceiptForm()
    return render(request, 'receipts/create_receipt.html', {'form': form})

@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(receipts__purchaser=request.user).distinct()
    category_data = []
    for category in categories:
        count = Receipt.objects.filter(category=category, purchaser=request.user).count()
        category_data.append({
            'name': category.name,
            'receipt_count': count
        })
    return render(request, 'receipts/category_list.html', {'categories': category_data})

@login_required
def account_list(request):
    accounts = Account.objects.filter(receipts__purchaser=request.user).distinct()
    account_data = []
    for account in accounts:
        count = Receipt.objects.filter(account=account, purchaser=request.user).count()
        account_data.append({
            'name': account.name,
            'number': account.number,
            'receipt_count': count
        })
    return render(request, 'receipts/account_list.html', {'accounts': account_data})

