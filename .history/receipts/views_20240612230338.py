from django.shortcuts import render, redirect
from .models import ExpenseCategory, Account, Receipt
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from .forms import ReceiptForm

@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {'receipts': receipts}
    return render(request, 'receipts/receipt_list.html', context)

@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')  # Redirect to the receipt list view
    else:
        form = ReceiptForm()
    return render(request, 'receipts/create_receipt.html', {'form': form})

Create the rest of the list views.

To access any list view, a person must be logged in.

For the list views, display the data in tables. Make sure that you filter the data for only the current user. You can do this by using the .filter method instead of the .all method.

things = Thing.objects.filter(user_property=request.user)
You'll need to refer to each model by what the name of the user_property is on that model. The one that's a foreign key to the User object.

Expense category list view
Path: receipts/categories/

For the ExpenseCategory list, have it show a table with columns for the name and the number of receipts associated to that category. An example would contain data that looks like this (but does not have to be formatted like this):

Name	Number of receipts
Gas	12
Entertainment	8
Novelty T-shirts	132
Mensa dues	0
You must register this view with the name "category_list".

Account list view
Path: receipts/accounts/

For the Account list, have it show a table with columns for the name, number, and the number of receipts associated to that account. An example would contain data that looks like this (but does not have to be formatted like this):

Name	Number	Number of receipts
Bank of America	12345678	0
Visa	1234 5678 9012 3456	88
You must register this view with the name "account_list".
