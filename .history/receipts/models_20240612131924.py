from django.db import models
from django.db.models.deletion import CASCADE
from django.contrib.auth.models import User

class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(User, related_name = "categories", on_delete=models.CASCADE)

    def __str__(self):
        return self.name

class Account(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(User, related_name = "accounts", on_delete=models.CASCADE)

    def __str__(self):
        return self.name

class Receipt(models.Model):
    vendor = models.CharField(max_length=200)

    def __str__(self):
        return self.description
