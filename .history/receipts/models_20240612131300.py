from django.db import models

class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50)
    owner = models.ForeignKey("auth.User", on_delete=models.CASCADE)

    def __str__(self):
        return self.name
