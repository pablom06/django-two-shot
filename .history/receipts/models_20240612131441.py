from django.db import models

class User(models.Model):

class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(User, "auth.User", on_delete=models.CASCADE)

    def __str__(self):
        return self.name
