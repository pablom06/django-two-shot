from django.contrib import admin
from .

class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'owner')

admin.site.register(EXpenseCategory, ExpenseCategoryAdmin)

class AccountAdmin(admin.ModelAdmin):
    list_display = ('name', 'number', 'owner')

class ReceiptAdmin(admin.ModelAdmin):
    list_display = ('vendor', 'total', 'tax', 'date', 'purchaser', 'category', 'account')
